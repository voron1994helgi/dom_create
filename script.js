/* 
Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

const footer = document.querySelector("footer");
const paragraph = footer.querySelector("p");

if (footer && paragraph) {
    const linkElement = document.createElement("a");
    linkElement.textContent = "Learn More";
    linkElement.href = "#";
    paragraph.append(linkElement);
}

const main = document.querySelector("main");

if (main) {
    const selectElement = document.createElement("select");
    selectElement.id = "rating";
    main.insertAdjacentElement("beforebegin", selectElement);

    const option4 = document.createElement("option");
    option4.value = "4";
    option4.textContent = "4 Stars";
    selectElement.appendChild(option4);

    const option3 = document.createElement("option");
    option3.value = "3";
    option3.textContent = "3 Stars";
    selectElement.appendChild(option3);

    const option2 = document.createElement("option");
    option2.value = "2";
    option2.textContent = "2 Stars";
    selectElement.appendChild(option2);

    const option1 = document.createElement("option");
    option1.value = "1";
    option1.textContent = "1 Star";
    selectElement.appendChild(option1);
}






